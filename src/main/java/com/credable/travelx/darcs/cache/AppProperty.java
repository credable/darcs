package com.credable.travelx.darcs.cache;

public enum AppProperty {

    LOCATION_RATE(AppConstants.LOCATION_RATE, "10"),
    LOG_MAILS(AppConstants.LOG_MAILS, "abhishek.trivedi@credable.in"),
    SERVICE_MAIL(AppConstants.SERVICE_MAIL, "travelxservice@gmail.com"),
    SERVICE_PASS(AppConstants.SERVICE_PASS, "gquditdulwoxqoyd"),
    PASSCODE(AppConstants.PASSCODE, "credable@01"),
    TRIP_SEND_TIME(AppConstants.TRIP_SEND_TIME, "10800");

    public static final AppProperty[] APP_PROPERTIES = values();
    private String name;
    private String value;

    private AppProperty(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public static String getValue(String name) {
        for (AppProperty appProperty : values()) {
            if (appProperty.name.equals(name)) {
                return appProperty.value;
            }
        }
        return null;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static AppProperty[] getAppProperties() {
        return APP_PROPERTIES;
    }

    @Override
    public String toString() {
        return name;
    }

}
