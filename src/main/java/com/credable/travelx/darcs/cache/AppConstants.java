package com.credable.travelx.darcs.cache;

public interface AppConstants {

    String LOCATION_RATE = "appLocationPacketRate";
    String LOG_MAILS = "appLogMail";
    String SERVICE_MAIL = "appServiceMail";
    String SERVICE_PASS = "appServicePass";
    String PASSCODE = "passcode";
    String APP_VERSION = "darcs.app.version";
    String MSG_DRIVER_NOT_FOUND = "darcs.msg.drivernotfound";
    String MSG_WRONG_OTP = "darcs.msg.wrongotp";
    String TRIP_SEND_TIME = "tripSendTime";

}
