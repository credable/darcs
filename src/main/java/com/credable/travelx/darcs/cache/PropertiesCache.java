package com.credable.travelx.darcs.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;

import com.credable.travelx.darcs.dao.SystemPropertyDAO;
import com.credable.model.SystemProperty;

@Configuration
@org.springframework.context.annotation.PropertySource("classpath:/application.properties")
public class PropertiesCache {

    @Autowired
    Environment env;
    Map<String, Object> allProps;

    @Autowired
    SystemPropertyDAO systemPropertyDAO;

    @PostConstruct
    public void loadAllProperties() {
        if (allProps == null) {
            allProps = new HashMap<>();
        }
        load();

    }

    public void load() {
        if (env instanceof ConfigurableEnvironment) {
            for (PropertySource<?> propertySource : ((ConfigurableEnvironment) env)
                .getPropertySources()) {
                if (propertySource instanceof EnumerablePropertySource) {
                    for (String key : ((EnumerablePropertySource<?>) propertySource)
                        .getPropertyNames()) {
                        allProps.put(key, propertySource.getProperty(key));
                    }
                }
            }
        }
        List<SystemProperty> systemProperties = systemPropertyDAO.getAll();
        for (SystemProperty systemProperty : systemProperties) {
            allProps.put(systemProperty.getName(), systemProperty.getValue());
        }

    }

    public void reload() {
        allProps.clear();
        load();
    }

    public String getProperties(String key) {
        if (allProps.containsKey(key)) {
            return allProps.get(key).toString();
        } else {
            return AppProperty.getValue(key);
        }
    }

}
