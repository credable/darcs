package com.credable.travelx.darcs.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.credable.model.Trip;

@Repository
public interface TripDAO extends JpaRepository<Trip, Integer> {

    @Query("select t from Trip t join t.operatorSubVendorDriver osd  where osd.driver.id = :driverId and t.plannedStartTime >= :laterThan and t.plannedStartTime <= :earlierTo and t.status >= :statusFrom and t.status < :statusTo order by t.plannedStartTime ASC")
    List<Trip> getTripsByStatusDriverIdAndTime(
        @Param(value = "driverId")
            int driverId,
        @Param(value = "laterThan")
            Date laterThan,
        @Param(value = "earlierTo")
            Date earlierTo,
        @Param(value = "statusFrom")
            int statusFrom,
        @Param(value = "statusTo")
            int statusTo);

    @Query("select t from Trip t where t.id = :tripId ")
    Trip findTripById(
        @Param(value = "tripId")
            int tripId);

    @Query("select t from  Trip t where t.id=:tripId")
    Trip getTripAndVehicle(
        @Param(value = "tripId")
            int tripId);

    // @Query(value = "select * from trip where actual_start_timestamp!=0 and vehicle_id = :vehicleId
    // order by id DESC limit 1", nativeQuery = true)
    // Trip getLastTripOnVehicle(@Param(value = "vehicleId") Integer vehicleId);

}
