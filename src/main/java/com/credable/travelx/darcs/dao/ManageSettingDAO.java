package com.credable.travelx.darcs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.credable.model.ManageSetting;

@Repository
public interface ManageSettingDAO extends JpaRepository<ManageSetting, Integer> {

    @Query("select m from ManageSetting m where m.operator.id = :operatorId")
    ManageSetting findByOperatorId(
        @Param(value = "operatorId")
            int operatorId);

}
