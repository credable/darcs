package com.credable.travelx.darcs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.credable.model.Vehicle;

@Repository
public interface VehicleDAO extends JpaRepository<Vehicle, Integer> {

}
