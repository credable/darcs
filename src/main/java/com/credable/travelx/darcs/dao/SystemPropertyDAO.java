package com.credable.travelx.darcs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.credable.model.SystemProperty;

import java.util.List;



@Repository
public interface SystemPropertyDAO extends JpaRepository<SystemProperty, String> {

    @Query("select sp from SystemProperty sp")
    List<SystemProperty> getAll();
}
