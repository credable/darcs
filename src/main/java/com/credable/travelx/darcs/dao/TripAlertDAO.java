package com.credable.travelx.darcs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.credable.model.TripAlert;

@Repository
public interface TripAlertDAO extends JpaRepository<TripAlert, Integer> {

    @Query("select ta from TripAlert ta where ta.id = :id and ta.vehicleId = :vehicleId")
    TripAlert getLastTripAlertOnVehicleByClassification(
        @Param(value = "id")
            int id,
        @Param(value = "vehicleId")
            Integer vehicleId);

}
