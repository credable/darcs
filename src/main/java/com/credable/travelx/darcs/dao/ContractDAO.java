package com.credable.travelx.darcs.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.credable.model.Contract;
import com.credable.model.ContractId;

@Repository
public interface ContractDAO extends JpaRepository<Contract, ContractId> {

    @Query("select c from Contract c where c.id.clientId = :clientId and c.id.operatorId = :operatorId and c.id.facilityId = :facilityId and c.id.zoneId = :zoneId and (c.id.partyId = :vendorId or c.id.partyId = :vehicleId)")
    List<Contract> getContractByVendorOrVehicle(
        @Param(value = "clientId")
            int clientId,
        @Param(value = "operatorId")
            int operatorId,
        @Param(value = "facilityId")
            int facilityId,
        @Param(value = "zoneId")
            int zoneId,
        @Param(value = "vendorId")
            String vendorId,
        @Param(value = "vehicleId")
            String vehicleId);

    @Query("select c from Contract c where c.id.contractId = :contractId")
    Contract getContractById(
        @Param(value = "contractId")
            Integer contractId);

}
