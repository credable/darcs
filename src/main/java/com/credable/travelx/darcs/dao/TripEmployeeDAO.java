package com.credable.travelx.darcs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.credable.model.TripEmployee;

@Repository
public interface TripEmployeeDAO extends JpaRepository<TripEmployee, Integer> {

}
