package com.credable.travelx.darcs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.credable.model.Driver;

@Repository
public interface DriverDAO extends JpaRepository<Driver, Integer> {

    @Query("select d from Driver d where d.webToken = :token")
    Driver findByWebToken(
        @Param(value = "token")
            String token);

    @Query("select d from Driver d where d.contactNumber = :contact")
    Driver findByContactNumber(
        @Param(value = "contact")
            String contact);

    @Query("select d from Driver d where d.id = :id")
    Driver findByDriverId(
        @Param(value = "id")
            int id);
}
