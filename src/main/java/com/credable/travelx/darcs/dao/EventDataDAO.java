package com.credable.travelx.darcs.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.credable.model.EventData;
import com.credable.model.EventDataPK;

@Repository
public interface EventDataDAO extends JpaRepository<EventData, EventDataPK> {

}
