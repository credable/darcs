package com.credable.travelx.darcs.pojo;

public class MetaDataWM {
    private String appLocationProvider;
    private int appGpsAccuracyThreshold;
    private boolean driverActive;
    private boolean appOutDated;
    private Integer locationPacketRate;

    public void setDriverActive(boolean active) {
        this.driverActive = active;
    }

    public boolean isDriverActive() {
        return this.driverActive;
    }

    public void setAppOutDated(boolean appOutDated) {
        this.appOutDated = appOutDated;
    }

    public boolean isAppOutDated() {
        return this.appOutDated;
    }

    public int getAppGpsAccuracyThreshold() {
        return appGpsAccuracyThreshold;
    }

    public void setAppGpsAccuracyThreshold(int appGpsAccuracyThreshold) {
        this.appGpsAccuracyThreshold = appGpsAccuracyThreshold;
    }



    public String getAppLocationProvider() {
        return appLocationProvider;
    }

    public void setAppLocationProvider(String appLocationProvider) {
        this.appLocationProvider = appLocationProvider;
    }

    public Integer getLocationPacketRate() {
        return locationPacketRate;
    }

    public void setLocationPacketRate(Integer locationPacketRate) {
        this.locationPacketRate = locationPacketRate;
    }
}
