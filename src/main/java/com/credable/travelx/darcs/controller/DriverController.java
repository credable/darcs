package com.credable.travelx.darcs.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.credable.model.Driver;
import com.credable.travelx.darcs.pojo.DriverWM;
import com.credable.travelx.darcs.service.DriverService;

@RestController
public class DriverController {

    @Autowired
    private DriverService driverService;

    private final static Logger logger = LoggerFactory.getLogger(DriverController.class);

    @RequestMapping("/")
    public String status() {
        return "UP";
    }

    @RequestMapping(value = "/update")
    public ResponseEntity<?> checkForAppUpdate(
        @RequestParam("id")
            int versionId) {
        logger.info("App  version " + versionId);
        return driverService.checkForAppUpdate(versionId);
    }

    @RequestMapping(value = "/driverlogs")
    public ResponseEntity<?> getDriverLogs(
        @RequestParam("driverId")
            int driverId,
        @RequestParam("duration")
            int duration) {
        return driverService.getDriverLogs(driverId, duration);
    }

    @RequestMapping(value = "/metadata")
    public ResponseEntity<?> sendMetaData(
        @RequestBody
            DriverWM driverWM) {
        return driverService.sendMetaData(driverWM);
    }

    @RequestMapping(value = "/driver/{contactNumber}/verify")
    public ResponseEntity<?> verifyDriver(
        @PathVariable("contactNumber")
            String contactNumber) {
        logger.info("Driver Contact Number " + contactNumber);
        return driverService.verifyDriver(contactNumber);
    }

    @RequestMapping(value = "/driver/{contactNumber}/forgotpassword")
    public ResponseEntity<?> forgotPassword(
        @PathVariable("contactNumber")
            String contactNumber) {
        logger.info("Driver Contact Number " + contactNumber);
        return driverService.forgotPassword(contactNumber);
    }

    @RequestMapping(value = "/driver/{contactNumber}/signup", consumes = "application/json", produces = "application/json", method = RequestMethod.PUT)
    public ResponseEntity<?> signUpDriver(
        @PathVariable("contactNumber")
            String contactNumber,
        @RequestBody
            DriverWM driverWM) {
        logger.info("Driver Contact Number " + contactNumber);
        return driverService.signUpDriver(contactNumber, driverWM);
    }

    @RequestMapping(value = "/driver/{contactNumber}/login", produces = "application/json", method = RequestMethod.POST)
    public ResponseEntity<?> loginDriver(
        @PathVariable("contactNumber")
            String contactNumber,
        @RequestBody
            DriverWM driverWM) {
        logger.info("Driver Contact Number " + contactNumber);
        return driverService.loginDriver(contactNumber, driverWM);
    }

    @RequestMapping(value = "/driver/logout", produces = "application/json")
    public ResponseEntity<?> logoutDriver(HttpServletRequest request) {
        Driver driver = (Driver) request.getAttribute("driver");
        return driverService.logoutDriver(driver);
    }

    @RequestMapping(value = "/driver/updatepassword", consumes = "application/json", produces = "application/json", method = RequestMethod.PUT)
    public ResponseEntity<?> setDriverPassword(
        @RequestBody
            DriverWM driverWM, HttpServletRequest request) {
        Driver driver = (Driver) request.getAttribute("driver");
        return driverService.setDriverPassword(driver, driverWM);
    }

    @RequestMapping(value = "/driver/token", consumes = "application/json", produces = "application/json", method = RequestMethod.PUT)
    public ResponseEntity<?> setDriverToken(
        @RequestBody
            DriverWM driverWM, HttpServletRequest request) {
        Driver driver = (Driver) request.getAttribute("driver");
        return driverService.setDriverToken(driver, driverWM);
    }
}
