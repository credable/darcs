package com.credable.travelx.darcs.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.credable.model.Driver;
import com.credable.travelx.darcs.cache.AppConstants;
import com.credable.travelx.darcs.cache.PropertiesCache;
import com.credable.travelx.darcs.service.UtilityService;

@RestController
public class UtilityController {

    @Autowired
    PropertiesCache propertiesCache;

    @Autowired
    UtilityService utilityService;

    @RequestMapping(value = "/reloadCache")
    ResponseEntity<?> reloadCache(
        @RequestParam("passcode")
            String passcode) {
        if (passcode.equals(propertiesCache.getProperties(AppConstants.PASSCODE))) {
            propertiesCache.reload();
            return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED.getReasonPhrase(),
                HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping(value = "/logsUpload")
    ResponseEntity<?> logsUpload(
        @RequestParam("files")
            List<MultipartFile> files,
        @RequestParam("detail")
            String detail, HttpServletRequest request) {
        utilityService.logsUpload(files, detail, (Driver) request.getAttribute("driver"));
        return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

    @PostMapping(value = "/appCrash")
    ResponseEntity<?> handleAppCrash(
        @RequestParam("stacktrace")
            String stackTrace, HttpServletRequest request) {
        utilityService.handleAppCrash(stackTrace, (Driver) request.getAttribute("driver"));
        return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

}
