package com.credable.travelx.darcs.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.credable.model.Driver;
import com.credable.travelx.darcs.pojo.TripWM;
import com.credable.travelx.darcs.service.TripEmployeeService;
import com.credable.travelx.darcs.service.TripService;

@RestController
public class TripController {

    @Autowired
    private TripService tripService;

    @Autowired
    private TripEmployeeService tripEmployeeService;

    @RequestMapping(value = "/driver/trips/active")
    public ResponseEntity<?> getActiveTripsByDriverId(HttpServletRequest request) {
        Driver driver = (Driver) request.getAttribute("driver");
        return tripService.getActiveTripsByDriverId(driver);
    }

    @RequestMapping(value = "/driver/trips/done")
    public ResponseEntity<?> getCompletedTripsByDriverId(HttpServletRequest request) {
        Driver driver = (Driver) request.getAttribute("driver");
        return tripService.getDoneTripsByDriverId(driver);
    }

    @RequestMapping(value = "/driver/trip/{tripId}", consumes = "application/json", produces = "application/json", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTrip(
        @PathVariable("tripId")
            int tripId,
        @RequestBody
            TripWM tripWM, HttpServletRequest request) {
        Driver driver = (Driver) request.getAttribute("driver");
        return tripService.updateTrip(tripId, driver, tripWM);
    }

    @RequestMapping(value = "/driver/trip/{tripId}/ack", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTripAck(
        @PathVariable("tripId")
            int tripId, HttpServletRequest request) {
        Driver driver = (Driver) request.getAttribute("driver");
        return tripService.updateTripAck(tripId, driver);
    }

    @RequestMapping(value = "/driver/trip/{tripId}/employee/{employeeId}", consumes = "application/json", produces = "application/json", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTripEmployee(
        @PathVariable("tripId")
            int tripId,
        @PathVariable("employeeId")
            String employeeId,
        @RequestBody
            TripWM tripWM, HttpServletRequest request) {
        Driver driver = (Driver) request.getAttribute("driver");
        return tripEmployeeService.updateTripEmployee(tripId, employeeId, driver, tripWM);
    }
}
