package com.credable.travelx.darcs.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.credable.travelx.darcs.cache.AppConstants;
import com.credable.travelx.darcs.cache.PropertiesCache;

@Service
public class Email {

    @Autowired
    PropertiesCache propertiesCache;

    public boolean send(String to, String subject, String message, List<String> attachements)
        throws Exception {
        Mail mail = new Mail(propertiesCache.getProperties(AppConstants.SERVICE_MAIL),
            propertiesCache.getProperties(AppConstants.SERVICE_PASS));
        if (subject != null && subject.length() > 0) {
            mail.setSubject(subject);
        } else {
            mail.setSubject("Subject");
        }

        if (message != null && message.length() > 0) {
            mail.setBody(message);
        } else {
            mail.setBody("Message");
        }

        mail.setTo(to.split(","));

        if (attachements != null) {
            for (String file : attachements) {
                mail.addAttachment(file);
            }
        }
        return mail.send();
    }

}
