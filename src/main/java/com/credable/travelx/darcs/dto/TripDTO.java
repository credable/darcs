package com.credable.travelx.darcs.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.credable.model.Trip;
import com.credable.model.TripEmployee;
import com.credable.util.TripType;

public class TripDTO {

    private int tripId;
    private Integer type;
    private int status;
    private Integer startTime;
    private Integer endTime;
    private String startLocation;
    private String endLocation;
    private String clientTripId;
    private String clientName;
    private String clientAddress;
    private String vehicle;
    private List<EmployeeDTO> empList;
    private Integer tripStartWindow;

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public String getClientTripId() {
        return clientTripId;
    }

    public void setClientTripId(String clientTripId) {
        this.clientTripId = clientTripId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public List<EmployeeDTO> getEmpList() {
        return empList;
    }

    public void setEmpList(List<EmployeeDTO> empList) {
        this.empList = empList;
    }

    public Integer getTripStartWindow() {
        return tripStartWindow;
    }

    public void setTripStartWindow(Integer tripStartWindow) {
        this.tripStartWindow = tripStartWindow;
    }

    public TripDTO map(Trip trip) {
        this.tripId = trip.getId();
        if (trip.getType() == TripType.PICK_UP.getValue()) {
            this.type = 1;
        } else {
            this.type = 2;
        }
        if (trip.getStatus() == 7)
            this.status = 8;
        else
            this.status = trip.getStatus();
        this.startTime = (int) (trip.getPlannedStartTime().getTime() / 1000);
        this.endTime = (int) (trip.getPlannedEndTime().getTime() / 1000);
        this.startLocation = trip.getStartLatitude() + "," + trip.getStartLongitude();
        this.endLocation = trip.getEndLatitude() + "," + trip.getEndLongitude();
        this.clientTripId = trip.getClientTripId();
        this.clientName = trip.getClient().getDisplayName();
        this.clientAddress = trip.getType() == TripType.PICK_UP.getValue() ?
            trip.getEndAddress() :
            trip.getStartAddress();
        this.vehicle = trip.getOperatorSubVendorVehicle().getVehicle().getRegistrationNumber();
        this.tripStartWindow = trip.getClient().getTripStartWindow();
        this.empList = new ArrayList<EmployeeDTO>();
        List<TripEmployee> allEmployeesOnTrip = trip.getTripEmployees();

        Collections.sort(allEmployeesOnTrip, new Comparator<TripEmployee>() {
            @Override
            public int compare(TripEmployee object1, TripEmployee object2) {
                return Integer.compare(object1.getSequenceNumber(), object2.getSequenceNumber());
            }
        });

        for (TripEmployee the : allEmployeesOnTrip)
            this.empList.add(new EmployeeDTO().map(the, trip));
        return this;
    }
}
