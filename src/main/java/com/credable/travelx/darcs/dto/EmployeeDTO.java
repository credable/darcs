package com.credable.travelx.darcs.dto;

import com.credable.model.Trip;
import com.credable.model.TripEmployee;
import com.credable.util.StatusCode;
import com.credable.util.TripType;

public class EmployeeDTO {

    private String empId;
    private String name;
    private byte gender;
    private String location;
    private String address;
    private Integer status;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte getGender() {
        return gender;
    }

    public void setGender(byte gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public EmployeeDTO map(TripEmployee the, Trip trip) {
        this.empId = the.getEmployee().getId().toString();
        this.name = the.getEmployee().getName();
        this.gender = the.getEmployee().getSex();
        if (trip.getType() == TripType.PICK_UP.getValue()) {
            this.location = the.getStartLatitude() + "," + the.getStartLongitude();
            this.address = the.getStartAddress();
        } else {
            this.location = the.getEndLatitude() + "," + the.getEndLongitude();
            this.address = the.getEndAddress();
        }

        if (the.getTrip().getStatus() != 0)
            this.status = the.getStatus();

        return this;
    }


}
