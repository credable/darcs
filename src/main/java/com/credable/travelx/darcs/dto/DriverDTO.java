package com.credable.travelx.darcs.dto;

import com.credable.model.Driver;

public class DriverDTO {
    private int driverId;
    private String name;
    private String photoUrl;
    private String licenceNumber;
    private String shiftCell;
    private String webToken;

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public String getShiftCell() {
        return shiftCell;
    }

    public void setShiftCell(String shiftCell) {
        this.shiftCell = shiftCell;
    }

    public String getWebToken() {
        return webToken;
    }

    public void setWebToken(String webToken) {
        this.webToken = webToken;
    }

    public DriverDTO map(Driver driver) {
        this.driverId = driver.getId();
        this.name = driver.getName();
        this.photoUrl = driver.getPhotoUrl();
        this.licenceNumber = driver.getLicenceNumber();
        this.webToken = driver.getWebToken();
        return this;
    }

}
