package com.credable.travelx.darcs.main;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.credable.travelx.darcs.service.AuthenticationService;

@Component
public class GlobalInterceptor implements HandlerInterceptor {

    @Autowired
    AuthenticationService authenticationService;

    private final static Logger logger = LoggerFactory.getLogger(GlobalInterceptor.class);

    GlobalInterceptor() {

    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
        Object handler) throws Exception {

        logger.info("URL : " + request.getRequestURL());
        String url = request.getServletPath();
        request.setAttribute("START_TIME", LocalDateTime.now());
        logger.info("Requested URL is : " + url);
        logger.info("Requested method is : " + request.getMethod());
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        logger.info("Remote IP address : " + ipAddress);
        if (authenticationService.isExculdedUrl(url)) {
            logger.info("URL is of type excluded pattern. No need to authenticate.");
            return true;
        } else {
            final String token = request.getHeader("x-auth-token");
            logger.info("Token is : " + token);
            if (token != null && !token.isEmpty()) {
                return authenticationService.validateApiRequest(request);
            } else {
                logger.error(
                    "Request not validated.Header info not available or invalid or unauthorized uuid available.");
                throw new HttpServerErrorException(HttpStatus.UNAUTHORIZED,
                    "Request not valid. Authorization key not valid.");
            }
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
        ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
        Object handler, Exception ex) throws Exception {
        request.setAttribute("userInfo", null);
        logger.info("Global interceptor -> afterCompletion called.");
        logger.info("Requested URL  : " + request.getServletPath());
        logger.info("Start Time " + request.getAttribute("START_TIME"));
        logger.info("Completion Time " + LocalDateTime.now());
        logger.info("Global interceptor -> afterCompletion finished.");
    }
}
