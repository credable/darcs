package com.credable.travelx.darcs.main;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.multipart.MultipartException;

import com.credable.travelx.darcs.cache.PropertiesCache;
import com.credable.util.EmailUtil;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @Autowired
    private PropertiesCache propertiesCache;

    @ExceptionHandler(HttpServerErrorException.class)
    @ResponseBody
    public ResponseEntity<?> handleException(HttpServletRequest request,
        HttpServletResponse httpRes, HttpServerErrorException ex) {
        httpRes.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        logger.error("UnauthorizedApiSessionException Occured:: URL=" + request.getRequestURL());
        logger.error(" Exception Message code" + ex.getStatusText(), ex);
        ResponseEntity<String> responseEntity =
            new ResponseEntity<String>(ex.getStatusCode().getReasonPhrase(), ex.getStatusCode());
        return responseEntity;
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<?> handleException(HttpServletRequest request,
        HttpServletResponse httpRes, Exception ex) {
        httpRes.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        logger.error("SessionException Occured:: URL=" + request.getRequestURL());
        logger.error(" Exception Message code" + ex.getMessage(), ex);
        ResponseEntity<String> responseEntity =
            new ResponseEntity<String>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        ex.printStackTrace(printWriter);
        EmailUtil.sendEmail(propertiesCache.getProperties("darcs.tech.emails"),
            "Something went wrong : " + propertiesCache.getProperties("darcs.server.tag"),
            stringWriter.toString());
        return responseEntity;
    }

    @ExceptionHandler(MultipartException.class)
    @ResponseBody
    ResponseEntity<?> handleControllerException(HttpServletRequest request, Throwable ex) {
        HttpStatus status = getStatus(request);
        return new ResponseEntity<String>(ex.getMessage(), status);

    }

    private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.valueOf(statusCode);
    }

}
