package com.credable.travelx.darcs.service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PushNotification {
    private static final String FIREBASE_SERVER_KEY =
        "AAAAbc7fU4A:APA91bH336S1vg9hHxHw99hV1w7Y3BSYlR-xnF4K-JHfUFAcBDWzLf7YFurUOZNxGkl2VOkPEG77vWVrpFAmAqOnr2vTsdDtoFeLhzP3Ndf4x6P6JY_ol--I8cFMhcoZhZx9oqaH3FLO";
    private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";

    public CompletableFuture<String> send(HttpEntity<String> entity) {
        RestTemplate restTemplate = new RestTemplate();
        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors
            .add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
        restTemplate.setInterceptors(interceptors);
        String firebaseResponse =
            restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);
        return CompletableFuture.completedFuture(firebaseResponse);
    }
}
