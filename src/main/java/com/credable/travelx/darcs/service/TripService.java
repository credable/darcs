package com.credable.travelx.darcs.service;

import com.credable.model.*;
import com.credable.travelx.darcs.cache.AppConstants;
import com.credable.travelx.darcs.cache.PropertiesCache;
import com.credable.travelx.darcs.dao.*;
import com.credable.travelx.darcs.dto.TripDTO;
import com.credable.travelx.darcs.pojo.TripWM;
import com.credable.util.Constants;
import com.credable.util.TripStatusCode;
import com.credable.util.VehicleType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class TripService {

    @Autowired
    private TripDAO tripDAO;

    @Autowired
    private ContractDAO contractDAO;

    @Autowired
    private EventDataDAO eventDataDAO;

    @Autowired
    private VehicleDAO vehicleDAO;

    @Autowired
    private DriverDAO driverDAO;

    private final static Logger logger = LoggerFactory.getLogger(TripService.class);

    @Autowired
    private PropertiesCache propertiesCache;

    @org.springframework.transaction.annotation.Transactional
    public ResponseEntity<?> getActiveTripsByDriverId(Driver driver) {
        if (driver == null)
            return new ResponseEntity<String>(
                propertiesCache.getProperties("darcs.msg.drivernotfound"), HttpStatus.NOT_FOUND);

        int currentTime = (int) (System.currentTimeMillis() / 1000);
        int laterThan = currentTime - Constants.TRIP_VISIBLITY_TIME_ON_VEHICLE;
        int earlierTo = currentTime + Integer
            .parseInt(propertiesCache.getProperties(AppConstants.TRIP_SEND_TIME));
        logger.info("earlierTo : " + earlierTo + " laterThan : " + laterThan);
        List<TripDTO> tripDTOs = new ArrayList<>();
        List<Trip> trips = tripDAO
            .getTripsByStatusDriverIdAndTime(driver.getId(), new Date((long) laterThan * 1000),
                new Date((long) earlierTo * 1000), TripStatusCode.STATUS_TRIP_ALLOCATED.getId(),
                TripStatusCode.STATUS_TRIP_FORCE_STOP.getId());
        if (trips != null && !trips.isEmpty()) {
            for (Trip trip : trips) {
                logger.info("Send trip " + trip.getId() + " for vehicle and vehicle Id is : " + trip
                    .getOperatorSubVendorVehicle().getVehicle().getId() + " Trip Status : " + trip
                    .getStatus());
                logger.info("Employee count: " + trip.getTripEmployees().size());
                tripDTOs.add(new TripDTO().map(trip));
            }
        }
        return new ResponseEntity<List<TripDTO>>(tripDTOs, HttpStatus.OK);
    }

    public ResponseEntity<?> updateTripAck(int tripId, Driver driver) {
        Trip trip = tripDAO.findTripById(tripId);
        if (trip == null)
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND);

        if (trip.getStatus() <= TripStatusCode.STATUS_TRIP_SENT.getId()) {
            trip.setStatus(TripStatusCode.STATUS_TRIP_NOTIFIED_BY_APP.getId());
            tripDAO.save(trip);
        }
        return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

    public ResponseEntity<?> updateTrip(int tripId, Driver driver, TripWM tripWM) {
        Trip trip = tripDAO.getTripAndVehicle(tripId);
        if (trip == null)
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND);
        TripStatus tripStatus = TripStatus.convert(tripWM.getStatus());
        processTripStatus(driver, trip, tripWM, tripStatus);
        return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

    private void processTripStatus(Driver driver, Trip trip, TripWM tripWM, TripStatus tripStatus) {
        TripStatusCode code = null;
        Vehicle vehicle =
            vehicleDAO.findOne(trip.getOperatorSubVendorVehicle().getVehicle().getId());

        switch (tripStatus) {
            case TRIP_ACK:
                code = TripStatusCode.STATUS_TRIP_NOTIFIED_BY_APP;
                break;
            case TRIP_START:
                if (trip.getActualStartTime() == null) {
                    code = TripStatusCode.STATUS_TRIP_START;
                    startTrip(vehicle, trip, tripWM, driver);
                    if (trip.getActualEndTime() != null)
                        code = TripStatusCode.getStatus(trip.getStatus());

                }
                break;
            case TRIP_STOP:
                if (trip.getActualEndTime() == null)
                    code = endTrip(driver, vehicle, trip, tripWM);
                break;
            default:
                break;
        }
        if (code != null) {
            trip.setStatus(code.getId());
            trip.setStatusTime(new Date((long) tripWM.getTimestamp() * 1000));

            EventDataPK id =
                new EventDataPK(code.getId(), new Date((long) tripWM.getTimestamp() * 1000),
                    driver.getId());
            EventData eventData = new EventData(id);
            String loc[] = tripWM.getLocation().split(",");
            eventData.setLatitude(Double.valueOf(loc[0]));
            eventData.setLongitude(Double.valueOf(loc[1]));
            eventDataDAO.save(eventData);

            vehicle.setTripStatus(code.getId());
            vehicleDAO.save(vehicle);
            driverDAO.save(driver);
            tripDAO.save(trip);
        }
    }

    private void startTrip(Vehicle vehicle, Trip trip, TripWM tripWM, Driver driver) {
        vehicle.setCurrentTrip(trip.getId());
        driver.setLastVehicleId(vehicle.getId());
        trip.setActualStartTime(new Date((long) tripWM.getTimestamp() * 1000));
        trip.setStartOdo((long) tripWM.getOdo());
        String loc[] = tripWM.getLocation().split(",");
        trip.setStartLatitude(Double.valueOf(loc[0]));
        trip.setStartLongitude(Double.valueOf(loc[1]));
    }

    private TripStatusCode endTrip(Driver driver, Vehicle vehicle, Trip trip, TripWM tripWM) {
        vehicle.setCurrentTrip(0);
        trip.setEndOdo((long) tripWM.getOdo());

        // add reason of unapproval
        double tripTotalMeters = trip.getEndOdo() - trip.getStartOdo();
        Contract contract = contractDAO.getContractById(trip.getContractId());
        boolean exp = false;
        if (contract.getPrimaryKms() == 0) {
            trip.setApprovedKm(trip.getSkc());
            if (trip.getApprovedKm() == 0) {
                exp = true;
                trip.setComment("SKC NOT FOUND");
            }

        } else
            trip.setApprovedKm(Math.round((tripTotalMeters / 1000.0) * 10) / 10.0f);

        VehicleType cap = VehicleType.getVehicleType(vehicle.getVehicleType().getSeatingCapacity());

        if (trip.getEmpCount() > cap.getCapacity()) {
            exp = true;
            trip.setComment("Employee count more than seating capacity.");
        }

        TripStatusCode status = TripStatusCode.STATUS_TRIP_KM_INCORRECT;
        if (tripTotalMeters > 0 && tripTotalMeters < 60000 && !exp) {
            if (tripWM.getAccuracy() > 15) {
                float checkKmValue = trip.getSkc();
                long diff = (long) Math.abs(checkKmValue - tripTotalMeters);
                if (diff < 0.50 * checkKmValue) {
                    status = TripStatusCode.STATUS_TRIP_APPROVED;
                    trip.setApprovedBy("SYSTEM");
                } else {
                    trip.setComment("GPS ISSUE");
                }
            } else {
                if (trip.getSkc() > 0) {
                    long diff = (long) Math.abs((long) (trip.getSkc() * 1000l) - tripTotalMeters);
                    if (diff < (0.50 * trip.getSkc() * 1000)) {
                        status = TripStatusCode.STATUS_TRIP_APPROVED;
                        trip.setApprovedBy("SYSTEM");
                    } else {
                        trip.setComment("KMS ERROR");
                    }
                } else {
                    status = TripStatusCode.STATUS_TRIP_APPROVED;
                    trip.setApprovedBy("SYSTEM");
                }
            }
        } else {
            if (!exp)
                trip.setComment("KMS > 60 or < 0");
        }
        logger.info("Trip meter : " + tripTotalMeters);

        trip.setActualEndTime(new Date((long) tripWM.getTimestamp() * 1000));
        String loc[] = tripWM.getLocation().split(",");
        trip.setEndLatitude(Double.valueOf(loc[0]));
        trip.setEndLongitude(Double.valueOf(loc[1]));

        long actualDistanceMeter = (long) (trip.getEndOdo() - trip.getStartOdo());
        return status;
    }


    @org.springframework.transaction.annotation.Transactional
    public ResponseEntity<?> getDoneTripsByDriverId(Driver driver) {
        if (driver == null)
            return new ResponseEntity<String>(
                propertiesCache.getProperties("darcs.msg.drivernotfound"), HttpStatus.NOT_FOUND);

        LocalDateTime localDateTime = LocalDateTime.now();
        int earlierTo =
            (int) localDateTime.toEpochSecond(ZoneOffset.of("+05:30")) + (5 * 60 * 60) + (30 * 60);
        localDateTime = LocalDate.now().minusDays(2).atStartOfDay();
        int laterThan = (int) localDateTime.toEpochSecond(ZoneOffset.of("+05:30"));
        logger.info("LT: " + laterThan + " ET: " + earlierTo);
        List<TripDTO> tripDTOs = new ArrayList<>();
        List<Trip> trips = tripDAO
            .getTripsByStatusDriverIdAndTime(driver.getId(), new Date((long) laterThan * 1000),
                new Date((long) earlierTo * 1000), TripStatusCode.STATUS_TRIP_FORCE_STOP.getId(),
                TripStatusCode.STATUS_TRIP_PAYABLE.getId());
        if (trips != null && !trips.isEmpty()) {
            for (Trip trip : trips) {
                logger.info(
                    "Send done trip " + trip.getId() + " for vehicle and vehicle Id is : " + trip
                        .getOperatorSubVendorVehicle().getVehicle().getId() + " Trip Status : "
                        + trip.getStatus());
                logger.info("the: " + trip.getTripEmployees().size());
                tripDTOs.add(new TripDTO().map(trip));
            }
        }
        return new ResponseEntity<List<TripDTO>>(tripDTOs, HttpStatus.OK);
    }


    public enum TripStatus {
        NO_TRIP,
        // 0
        TRIP_ACK,
        // 1
        TRIP_DRIVER_SWIPE,
        // 2
        TRIP_OTP_VERIFIED,
        // 3
        TRIP_START,
        // 4
        TRIP_FORCE_START,
        // 6
        TRIP_ENTER_START_GEOZONE,
        // 7
        TRIP_EXIT_START_GEOZONE,
        // 8
        TRIP_ENTER_STOP_GEOZONE,
        // 9
        TRIP_EXIT_STOP_GEOZONE,
        // a
        TRIP_STOP,
        // b
        TRIP_FORCE_STOP,
        // c
        TRIP_SYNC,
        // d
        TRIP_FORCE_END_WEBAPP; // e

        static final TripStatus[] TRIP_STATUS = values();

        public static TripStatus convert(final int value) {
            return TRIP_STATUS[value];
        }
    }

}

