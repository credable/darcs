package com.credable.travelx.darcs.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.credable.alert.AlertClassification;
import com.credable.alert.AlertStatus;
import com.credable.model.Trip;
import com.credable.model.TripAlert;
import com.credable.model.TripEmployee;
import com.credable.travelx.darcs.dao.TripAlertDAO;

@Service
public class AlertService {

    @Autowired
    TripAlertDAO tripAlertDAO;

    public void generateTripAlert(Trip trip, AlertClassification alertClassification,
        String comment, String message) {
        if (alertClassification.isSnoozable()) {
            TripAlert lastAlert = tripAlertDAO
                .getLastTripAlertOnVehicleByClassification(alertClassification.getId(),
                    trip.getOperatorSubVendorVehicle().getVehicle().getId());
            if (lastAlert != null && lastAlert.getSnoozeStartTime().getTime() < System
                .currentTimeMillis() && lastAlert.getSnoozeEndTime().getTime() >= System
                .currentTimeMillis())
                return;
        }
        TripAlert alert = new TripAlert();
        int timestamp = (int) (System.currentTimeMillis() / 1000);
        alert.setComment(comment);
        alert.setStatus((byte) AlertStatus.GENERATED.getId());
        alert.setDisposeTime(new Date(((long) timestamp + (8 * 60 * 60)) * 1000));
        alert.setMessage(message);
        alert.setTitle(alertClassification.getTitle());
        alert.setClientId(trip.getClient().getId());
        alert.setClientTripId(trip.getClientTripId());
        tripAlertDAO.save(alert);
    }

    public void generateTripEmployeeAlert(Trip trip, TripEmployee employee,
        AlertClassification alertClassification, String comment, String message) {
        if (alertClassification.isSnoozable()) {
            TripAlert lastAlert = tripAlertDAO
                .getLastTripAlertOnVehicleByClassification(alertClassification.getId(),
                    trip.getOperatorSubVendorVehicle().getVehicle().getId());
            if (lastAlert != null && lastAlert.getSnoozeStartTime().getTime() < System
                .currentTimeMillis() && lastAlert.getSnoozeEndTime().getTime() >= System
                .currentTimeMillis())
                return;
        }
        TripAlert alert = new TripAlert();
        int timestamp = (int) (System.currentTimeMillis() / 1000);
        alert.setComment(comment);
        alert.setStatus((byte) AlertStatus.GENERATED.getId());
        alert.setDisposeTime(new Date(((long) timestamp + (8 * 60 * 60)) * 1000));
        alert.setMessage(message);
        alert.setTitle(alertClassification.getTitle());
        alert.setClientId(trip.getClient().getId());
        alert.setClientTripId(trip.getClientTripId());
        tripAlertDAO.save(alert);
    }

}
