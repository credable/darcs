package com.credable.travelx.darcs.service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.credable.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.credable.alert.AlertClassification;
import com.credable.model.Driver;
import com.credable.model.Employee;
import com.credable.model.EventData;
import com.credable.model.EventDataPK;
import com.credable.model.ManageSetting;
import com.credable.model.Trip;
import com.credable.model.TripEmployee;
import com.credable.travelx.darcs.dao.EventDataDAO;
import com.credable.travelx.darcs.dao.ManageSettingDAO;
import com.credable.travelx.darcs.dao.TripDAO;
import com.credable.travelx.darcs.dao.TripEmployeeDAO;
import com.credable.travelx.darcs.pojo.TripWM;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class TripEmployeeService {

    @Autowired
    private TripDAO tripDAO;

    @Autowired
    private TripEmployeeDAO tripHasEmployeeDAO;

    @Autowired
    private ManageSettingDAO manageSettingDAO;

    @Autowired
    private AlertService alertService;

    @Autowired
    private EventDataDAO eventDataDAO;

    @Autowired
    private PushNotification pushNotification;

    private final static Logger logger = LoggerFactory.getLogger(TripEmployeeService.class);


    public ResponseEntity<?> updateTripEmployee(int tripId, String employeeId, Driver driver,
        TripWM tripWM) {
        Trip trip = tripDAO.findTripById(tripId);
        if (trip == null)
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND);

        EmpStatus empStatus = EmpStatus.convert(tripWM.getStatus());

        processTripEmployeeStatus(trip, tripWM, empStatus, employeeId, driver);
        return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }


    private void processTripEmployeeStatus(Trip trip, TripWM tripWM, EmpStatus empStatus,
        String empId, Driver driver) {
        List<TripEmployee> tripHasEmployees = trip.getTripEmployees();
        ManageSetting manageSetting = manageSettingDAO.findByOperatorId(trip.getOperator().getId());
        LocalDateTime dateTime = LocalDateTime.now(ZoneId.of("Asia/Kolkata"));
        logger.info("Hour of the day : " + dateTime.getHour());
        int tripId = trip.getId();
        boolean nightShift = dateTime.getHour() >= 19 || dateTime.getHour() < 7;
        logger.info("Night Shift value : " + nightShift);
        TripEmployee tripEmployee =
            tripHasEmployees.stream().filter(the -> the.getEmployee().getId().equals(Integer
                .parseInt(empId)))
                .findFirst().orElse(null);
        if (tripEmployee != null && manageSetting != null) {
            EventDataPK id =
                new EventDataPK(empStatus.ordinal(), new Date((long) tripWM.getTimestamp() * 1000),
                    driver.getId());
            EventData eventData = new EventData(id);
            eventData.setDescription(empId);
            String loc[] = tripWM.getLocation().split(",");
            eventData.setLatitude(Double.valueOf(loc[0]));
            eventData.setLongitude(Double.valueOf(loc[1]));
            Employee employee = tripEmployee.getEmployee();
            String message = null;
            int clientId = trip.getClient().getId();
            String clientTripId = trip.getClientTripId();
            String lastAddress = tripWM.getAddress();

            switch (empStatus) {
                case BOARDED:
                    if (tripEmployee.getTrip().getType() == TripType.PICK_UP.getValue())
                        firstTimeBoarded(trip, tripEmployee, employee, lastAddress, eventData);

                    tripEmployee.setNotificationStatus(Constants.EMP_BOARDED_OR_MISSED.getId());
                    tripEmployee.setStartOdo((long) tripWM.getOdo());
                    tripEmployee.setActualStartTime(new Date((long) tripWM.getTimestamp() * 1000));
                    tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMPLOYEE_BOARDED.getId());
                    if (trip.getType() == TripType.DROP.getValue()) {
                        eventData.setAddress(trip.getStartAddress());
                    } else {
                        eventData.setAddress(
                            employee.getAddressLine1() + " " + employee.getAddressLine2() + " "
                                + employee.getAddressCity() + " " + employee.getAddressState());
                    }
                    break;
                case DEBOARDED:
                    if (tripEmployee.getTrip().getType() == TripType.DROP.getValue())
                        firstTimeDeboarded(trip, tripEmployee, employee, lastAddress, eventData);

                    tripEmployee.setEndOdo((long) tripWM.getOdo());
                    tripEmployee.setActualEndTime(new Date((long) tripWM.getTimestamp() * 1000));
                    tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMPLOYEE_DEBOARDED.getId());
                    if (trip.getType() == TripType.PICK_UP.getValue()) {
                        eventData.setAddress(trip.getEndAddress());
                    } else {
                        eventData.setAddress(
                            employee.getAddressLine1() + " " + employee.getAddressLine2() + " "
                                + employee.getAddressCity() + " " + employee.getAddressState());
                    }
                    break;
                case MISSING:
                    if (tripEmployee.getStatus() != EmployeeStatusCode.STATUS_EMP_NOSHOW.getId()) {
                        message =
                            " Employee " + employee.getName() + "(" + tripEmployee.getEmployee()
                                .getId() + ")  did not board vehicle" + "(" + trip
                                .getOperatorSubVendorVehicle().getVehicle().getId()
                                + ") scheduled for Trip (" + tripId + ")";
                        clientId = trip != null ? trip.getClient().getId() : 0;
                        clientTripId = trip != null ? trip.getClientTripId() : "NA";
                        alertService.generateTripEmployeeAlert(trip, tripEmployee,
                            AlertClassification.ALERT_EMP_MISSING, null, message);
                        if (employee.getLineManagerEmail() != null && !employee
                            .getLineManagerEmail().isEmpty())
                            EmailUtil.sendEmail(employee.getLineManagerEmail(), "Employee No Show",
                                message);

                    }
                    tripEmployee.setNotificationStatus(Constants.EMP_BOARDED_OR_MISSED.getId());
                    tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMP_NOSHOW.getId());
                    tripEmployee.setStartOdo((long) tripWM.getOdo());
                    tripEmployee.setEndOdo((long) tripWM.getOdo());
                    tripEmployee.setExitGeozoneTime(eventData.getId().getTimestamp());
                    checkForAloneFemaleIncaseOfMiss(trip, tripHasEmployees, nightShift, employee);
                    break;
                case ENTER_GEOZONE:
                    eventData.getId()
                        .setStatusCode(EmployeeStatusCode.STATUS_EMPLOYEE_LOC_ENTER.getId());
                    eventData.setAddress(
                        employee.getAddressLine1() + " " + employee.getAddressLine2() + " "
                            + employee.getAddressCity() + " " + employee.getAddressState());
                    tripEmployee.setEnterGeozoneTime(eventData.getId().getTimestamp());
                    break;
                case EXIT_GEOZONE:
          /*
           * if (tripHasEmployees.size() <= 8 && (tripHasEmployee.getNotificationStatus() & 0x1) ==
           * 0) { if (tripHasEmployee.getId().getTripType() == Constants.TRIP_DROP.getId() &&
           * EmpStatus.isBoarded(tripHasEmployee.getStatus())) { if (tripHasEmployee.getStatus() !=
           * StatusCode.STATUS_EMPLOYEE_DEBOARDED.getId() || tripHasEmployee.getStatus() !=
           * StatusCode.STATUS_EMP_FORCE_DEBOARD.getId() || tripHasEmployee.getStatus() !=
           * StatusCode.STATUS_EMP_NOZONE_DEBOARD.getId() || tripHasEmployee.getStatus() !=
           * StatusCode.STATUS_EMP_OUTZONE_DEBOARD.getId() || tripHasEmployee.getStatus() !=
           * StatusCode.STATUS_EMP_NOSHOW.getId() || tripHasEmployee.getStatus() !=
           * StatusCode.STATUS_EMP_FORCE_MISSING.getId()) {
           * 
           * message = "Employee " + tripHasEmployee.getEmployee().getName() + " (" +
           * tripHasEmployee.getId().getEmployeeEmployeeId() + ") By-passed by Driver for Trip   " +
           * tripId + "in vehicle " +
           * trip.getOperatorSubVendorVehicle().getVehicle().getRegistrationNumber (); clientId =
           * trip != null ? trip.getClient().getId() : 0; clientTripId = trip != null ?
           * trip.getClientTripId() : "NA"; alertService.generateTripEmployeeAlert(trip,
           * tripHasEmployee, AlertClassification.ALERT_EMP_BYPASSED, null, message);
           * tripHasEmployee.setNotificationStatus(Utility.setBit(tripHasEmployee.
           * getNotificationStatus(), 1)); } } else if (tripHasEmployee.getId().getTripType() ==
           * Constants.TRIP_PICKUP.getId() && (tripHasEmployee.getStatus() == 0 ||
           * tripHasEmployee.getStatus() == StatusCode.STATUS_EMP_BOARD_INTIMATE.getId())) { message
           * = "Employee " + tripHasEmployee.getEmployee().getName() + " (" +
           * tripHasEmployee.getId().getEmployeeEmployeeId() + ") By-passed by Driver for Trip   " +
           * tripId + "in vehicle " +
           * trip.getOperatorSubVendorVehicle().getVehicle().getRegistrationNumber (); clientId =
           * trip != null ? trip.getClient().getId() : 0; clientTripId = trip != null ?
           * trip.getClientTripId() : "NA"; alertService.generateTripEmployeeAlert(trip,
           * tripHasEmployee, AlertClassification.ALERT_EMP_BYPASSED, null, message);
           * tripHasEmployee.setNotificationStatus(Utility.setBit(tripHasEmployee.
           * getNotificationStatus(), 1)); } }
           */
                    eventData.getId()
                        .setStatusCode(EmployeeStatusCode.STATUS_EMPLOYEE_LOC_LEAVE.getId());
                    eventData.setAddress(
                        employee.getAddressLine1() + " " + employee.getAddressLine2() + " "
                            + employee.getAddressCity() + " " + employee.getAddressState());
                    tripEmployee.setExitGeozoneTime(eventData.getId().getTimestamp());
                    if ((trip.getType() == TripType.PICK_UP.getValue()) &&
                        tripEmployee.getExitGeozoneTime().getTime() - tripEmployee
                            .getEnterGeozoneTime().getTime()
                            < (long) (Constants.DRIVER_WAIT_TIME_IN_EMP_GEOZONE.getId()) && (
                        trip.getStatus() >= TripStatusCode.STATUS_TRIP_START.getId()
                            && trip.getStatus() <= TripStatusCode.STATUS_TRIP_FORCE_STOP.getId())
                        && !(EmpStatus.isBoarded(tripEmployee.getStatus()) || EmpStatus
                        .isMissed(tripEmployee.getStatus()) || !(EmpStatus
                        .isDeboarded(tripEmployee.getStatus())))) {
                        logger.info(
                            "Driver wait less than 3 mins and geozone distance " + tripEmployee
                                .getEmployee().getId() + " " + employee.getName());
                        message = "Driver wait less than 3 min. in Emp " + employee.getName() + "("
                            + tripEmployee.getEmployee().getId() + ") Geozone on vehicle " + trip
                            .getOperatorSubVendorVehicle().getVehicle().getRegistrationNumber()
                            .toUpperCase(Locale.ENGLISH);
                        alertService.generateTripEmployeeAlert(trip, tripEmployee,
                            AlertClassification.ALERT_DRIVER_WAIT_LESS_THAN_3_MIN, null, message);
                    }
                    break;
                case WRONG_BOARDEDED:
                    if (tripEmployee.getTrip().getType() == TripType.PICK_UP.getValue()) {
                        if (!firstTimeBoarded(trip, tripEmployee, employee, lastAddress,
                            eventData)) {
                            if (tripEmployee.getStatus()
                                != EmployeeStatusCode.STATUS_EMP_OUTZONE_BOARD.getId()) {
                                message = " Emp " + employee.getName() + "(" + employee.getId()
                                    + ") boarded vehicle (" + trip.getOperatorSubVendorVehicle()
                                    .getVehicle().getRegistrationNumber() + ")" + " at location ("
                                    + tripWM.getAddress() + "), outside Geo zone.";
                                clientId = trip != null ? trip.getClient().getId() : 0;
                                clientTripId = trip != null ? trip.getClientTripId() : "NA";
                                alertService.generateTripEmployeeAlert(trip, tripEmployee,
                                    AlertClassification.ALERT_EMP_OUTZONE_BOARD, null, message);
                                tripEmployee.setStartLatitude(eventData.getLatitude());
                                tripEmployee.setStartLongitude(eventData.getLongitude());
                            }
                        }
                        eventData.getId()
                            .setStatusCode(EmployeeStatusCode.STATUS_EMP_OUTZONE_BOARD.getId());
                        tripEmployee.setStartOdo((long) tripWM.getOdo());
                        tripEmployee
                            .setActualStartTime(new Date((long) tripWM.getTimestamp() * 1000));
                        tripEmployee.setNotificationStatus(Constants.EMP_BOARDED_OR_MISSED.getId());
                        tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMP_OUTZONE_BOARD.getId());
                    }
                    break;
                case WRONG_DEBOARDEDED:
                    if ((tripEmployee.getTrip().getType() == TripType.DROP.getValue())) {
                        // TODO Drop LatLong
                        if (!firstTimeDeboarded(trip, tripEmployee, employee, lastAddress,
                            eventData)) {
                            if (tripEmployee.getStatus()
                                != EmployeeStatusCode.STATUS_EMP_OUTZONE_DEBOARD.getId()) {
                                message = " Emp " + employee.getName() + "(" + employee.getId()
                                    + ") de-boarded vehicle (" + trip.getOperatorSubVendorVehicle()
                                    .getVehicle().getRegistrationNumber() + ")" + " at location ("
                                    + tripWM.getAddress() + "), outside Geo zone.";
                                clientId = trip != null ? trip.getClient().getId() : 0;
                                clientTripId = trip != null ? trip.getClientTripId() : "NA";
                                alertService.generateTripEmployeeAlert(trip, tripEmployee,
                                    AlertClassification.ALERT_EMP_OUTZONE_DEBOARD, null, message);
                                tripEmployee.setEndLatitude(eventData.getLatitude());
                                tripEmployee.setEndLongitude(eventData.getLongitude());
                            }
                        }
                    }
                    eventData.getId()
                        .setStatusCode(EmployeeStatusCode.STATUS_EMP_OUTZONE_DEBOARD.getId());
                    tripEmployee.setEndOdo((long) tripWM.getOdo());
                    tripEmployee.setActualEndTime(new Date((long) tripWM.getTimestamp() * 1000));
                    tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMP_OUTZONE_DEBOARD.getId());
                    break;
                case NO_ZONE_BOARDEDED:
                    if (tripEmployee.getTrip().getType() == TripType.PICK_UP.getValue()) {
                        if (employee.getValidPickLatLong() < 2) {
                            firstTimeBoarded(trip, tripEmployee, employee, lastAddress, eventData);
                        } else {
                            if (tripEmployee.getStatus()
                                != EmployeeStatusCode.STATUS_EMP_NOZONE_BOARD.getId()) {

                                message = " Emp " + employee.getName() + "(" + employee.getId()
                                    + ") boarded vehicle (" + trip.getOperatorSubVendorVehicle()
                                    .getVehicle().getRegistrationNumber() + ")" + " at location ("
                                    + tripWM.getAddress() + "), outside Geo zone.";
                                alertService.generateTripEmployeeAlert(trip, tripEmployee,
                                    AlertClassification.ALERT_EMP_NOZONE_BOARD, null, message);

                                tripEmployee.setStartLatitude(eventData.getLatitude());
                                tripEmployee.setStartLongitude(eventData.getLongitude());
                            }
                        }
                    }
                    eventData.getId()
                        .setStatusCode(EmployeeStatusCode.STATUS_EMP_NOZONE_BOARD.getId());
                    tripEmployee.setStartOdo((long) tripWM.getOdo());
                    tripEmployee.setActualStartTime(new Date((long) tripWM.getTimestamp() * 1000));
                    tripEmployee.setNotificationStatus(Constants.EMP_BOARDED_OR_MISSED.getId());
                    tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMP_NOZONE_BOARD.getId());
                    break;
                case NO_ZONE_DEBOARDED:
                    if ((tripEmployee.getTrip().getType() == TripType.DROP.getValue())) {
                        // TODO Drop LatLong
                        if (!firstTimeDeboarded(trip, tripEmployee, employee, lastAddress,
                            eventData)) {
                            if (tripEmployee.getStatus()
                                != EmployeeStatusCode.STATUS_EMP_NOZONE_DEBOARD.getId()) {
                                message = " Emp " + employee.getName() + "(" + employee.getId()
                                    + ") de-boarded vehicle (" + trip.getOperatorSubVendorVehicle()
                                    .getVehicle().getRegistrationNumber() + ")" + " at location ("
                                    + tripWM.getAddress() + "), outside Geo zone.";
                                clientId = trip != null ? trip.getClient().getId() : 0;
                                clientTripId = trip != null ? trip.getClientTripId() : "NA";
                                alertService.generateTripEmployeeAlert(trip, tripEmployee,
                                    AlertClassification.ALERT_EMP_NOZONE_DEBOARD, null, message);
                                tripEmployee.setEndLatitude(eventData.getLatitude());
                                tripEmployee.setEndLongitude(eventData.getLongitude());
                            }
                        }
                    }
                    eventData.getId()
                        .setStatusCode(EmployeeStatusCode.STATUS_EMP_NOZONE_DEBOARD.getId());
                    tripEmployee.setEndOdo((long) tripWM.getOdo());
                    tripEmployee.setActualEndTime(eventData.getId().getTimestamp());
                    tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMP_NOZONE_DEBOARD.getId());
                    break;
                case INTIMATION:
                    tripEmployee.setIntimationTime(new Date());
                    eventData.getId().setTimestamp(new Date(
                        eventData.getId().getTimestamp().getTime() - tripEmployee
                            .getSequenceNumber()));
                    if (tripEmployee.getNotificationStatus()
                        < Constants.SECOND_NOTIFICATION_DILIVERED.getId()
                        && tripEmployee.getTrip().getType() == TripType.PICK_UP.getValue()
                        && tripEmployee.getSequenceNumber() != 1) {
                        String lastDriverName =
                            trip.getOperatorSubVendorDriver().getDriver().getName();
                        if (lastDriverName != null && lastDriverName.length() > 15)
                            lastDriverName = lastDriverName.substring(0, 15);
                        else
                            lastDriverName = "NA";
                        // String empName[] = employee.getName().split(" ");
                        // String recipients = "" + tripHasEmployee.getEmployee().getContactPhone();
                        // String sms = "(" +
                        // trip.getOperatorSubVendorVehicle().getVehicle().getRegistrationNumber()
                        // + " /" + lastDriverName
                        // + ") is in your vicinity and will reach your location/pickup point in next 3-5
                        // minutes. It will
                        // move after waiting for 3 minutes.";
                        // String msg = "Dear " + empName[0].toUpperCase() + "," + "<br></br>" + "(" +
                        // trip.getOperatorSubVendorVehicle().getVehicle().getRegistrationNumber()
                        // + " / driver " + lastDriverName
                        // + ") is in your vicinity & will reach your " +
                        // (employee.getAddressLine1().toUpperCase() != null
                        // ? employee.getAddressLine1().toUpperCase() : "NA") + " "
                        // + (employee.getAddressLine2().toUpperCase() != null ?
                        // employee.getAddressLine2().toUpperCase() :
                        // "NA")
                        // + " in next 3-5 minutes. It will move after waiting for 3 minutes." + "
                        // <br></br><br></br>" + "
                        // Cloud One Command Center";
                        // String fromEmail = "services@cloud1.in";
                        // String emailrecipients = "" + employee.getEmail() != null ? employee.getEmail() : "";
                        // String emailSubject = "Your Pick Up Notification";
                        // String emailBody = msg;
                        // EmailUtil.sendEmail(emailrecipients, emailSubject, emailBody, fromEmail,null,null);
                        // Client client = repo.getClientByClientId(trip.getClient().getId());
                        // if (client != null && client.isSmsSubscription() && !client.isIvrSubscription()) {
                        // Utility.sendSms(recipients, sms, false);
                        // }
                        tripEmployee
                            .setNotificationStatus(Constants.SECOND_NOTIFICATION_COMPLETED.getId());
                    }
                    break;
                case FORCE_INTIMATION:
                    sendEmployeeNotification(employee);
                    break;
                case FORCE_BOARDED:
                    if (tripHasEmployees.size() <= 8 && (tripEmployee.getStatus()
                        != EmployeeStatusCode.STATUS_EMP_FORCE_BOARD.getId())) {
                        message = " Emp " + employee.getName() + " (" + employee.getId()
                            + ") has been force boarded into " + "(" + trip
                            .getOperatorSubVendorVehicle().getVehicle().getRegistrationNumber()
                            + ")" + " at location (" + tripWM.getAddress() + ")";
                        clientId = trip.getClient().getId();
                        clientTripId = trip.getClientTripId();
                        alertService.generateTripEmployeeAlert(trip, tripEmployee,
                            AlertClassification.ALERT_EMP_FORCE_BOARD, null, message);

                        tripEmployee.setStartLatitude(eventData.getLatitude());
                        tripEmployee.setStartLongitude(eventData.getLongitude());
                    }
                    eventData.getId()
                        .setStatusCode(EmployeeStatusCode.STATUS_EMP_FORCE_BOARD.getId());
                    tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMP_FORCE_BOARD.getId());
                    tripEmployee.setStartOdo((long) tripWM.getOdo());
                    tripEmployee.setNotificationStatus(Constants.EMP_BOARDED_OR_MISSED.getId());
                    tripEmployee.setActualStartTime(eventData.getId().getTimestamp());
                    break;
                case FORCE_DEBOARDED:
                    if (tripHasEmployees.size() <= 8 && (tripEmployee.getStatus()
                        != EmployeeStatusCode.STATUS_EMP_FORCE_DEBOARD.getId())) {
                        message = " Emp " + employee.getName() + "(" + employee.getId()
                            + ") has been force de-boarded from " + "(" + trip
                            .getOperatorSubVendorVehicle().getVehicle().getRegistrationNumber()
                            + ")" + " at location (" + tripWM.getAddress() + ")";
                        clientId = trip != null ? trip.getClient().getId() : 0;
                        clientTripId = trip != null ? trip.getClientTripId() : "NA";
                        alertService.generateTripEmployeeAlert(trip, tripEmployee,
                            AlertClassification.ALERT_EMP_FORCE_DEBOARD, null, message);
                        tripEmployee.setEndLatitude(eventData.getLatitude());
                        tripEmployee.setEndLongitude(eventData.getLongitude());
                    }
                    eventData.getId()
                        .setStatusCode(EmployeeStatusCode.STATUS_EMP_FORCE_DEBOARD.getId());
                    tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMP_FORCE_DEBOARD.getId());
                    tripEmployee.setEndOdo((long) tripWM.getOdo());
                    tripEmployee.setActualEndTime(eventData.getId().getTimestamp());
                    break;
                case FORCE_MISSING:
                    String type = null;
                    if (tripHasEmployees.size() <= 8 && (tripEmployee.getStatus()
                        != EmployeeStatusCode.STATUS_EMP_FORCE_MISSING.getId())) {
                        String date = null;
                        if (tripEmployee.getTrip().getType() == TripType.PICK_UP.getValue()) {
                            type = "Pick Up";
                            date = Utility.formatTimestamp(trip.getPlannedEndTime().getTime(),
                                "dd/mm/yyyy HH:mm");
                        } else {
                            type = "Drop";
                            date = Utility.formatTimestamp(trip.getPlannedStartTime().getTime(),
                                "dd/mm/yyyy HH:mm");
                        }
                        message = "Employee " + employee.getName() + "(" + employee.getId()
                            + ") has cancelled the scheduled ride for trip (" + type + ") dated ("
                            + date + ") trip (" + tripId + ") on vehicle (" + trip
                            .getOperatorSubVendorVehicle().getVehicle().getRegistrationNumber()
                            + ").";
                        clientId = trip != null ? trip.getClient().getId() : 0;
                        clientTripId = trip != null ? trip.getClientTripId() : "NA";
                        alertService.generateTripEmployeeAlert(trip, tripEmployee,
                            AlertClassification.ALERT_EMP_VALIDATION_FAILED, null, message);
                    }
                    eventData.getId()
                        .setStatusCode(EmployeeStatusCode.STATUS_EMP_FORCE_MISSING.getId());
                    tripEmployee.setNotificationStatus(Constants.EMP_BOARDED_OR_MISSED.getId());
                    tripEmployee.setStatus(EmployeeStatusCode.STATUS_EMP_FORCE_MISSING.getId());
                    // Set Startodo and endodo Missing case
                    tripEmployee.setStartOdo((long) tripWM.getOdo());
                    tripEmployee.setEndOdo((long) tripWM.getOdo());
                    checkForAloneFemaleIncaseOfMiss(trip, tripHasEmployees, nightShift, employee);
                    break;
                default:
                    break;
            }
            eventData.setDescription(tripEmployee.getEmployee().getId().toString());

            eventDataDAO.save(eventData);
            tripHasEmployeeDAO.save(tripEmployee);
            tripDAO.save(trip);
        }
    }

    private void sendEmployeeNotification(Employee employee) {
        try {
            if (employee.getDeviceToken() == null) {
                logger.info("No TOKEN FOUND FOR EMPLOYEE " + employee.getId());
                return;
            }

            ObjectMapper mapper = new ObjectMapper();
            ObjectNode body = mapper.createObjectNode();
            body.put("to", employee.getDeviceToken());
            body.put("priority", "high");
            ObjectNode notif = mapper.createObjectNode();
            ObjectNode data = mapper.createObjectNode();
            notif.put("title", "Cab has arrived");
            notif.put("body",
                "Cab has arrived in your vicinity. Please reach your pickup location.");
            body.set("data", data);
            body.set("notification", notif);
            HttpEntity<String> request = new HttpEntity<>(body.toString());
            pushNotification.send(request);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
        }
    }


    private void checkForAloneFemaleIncaseOfMiss(Trip trip, List<TripEmployee> orderedEmployeeList,
        boolean nightShift, Employee thisEmp) {
        String message;
        if (orderedEmployeeList != null) {
            if (thisEmp.getSex() == 0 && trip.getSecurityRequired() && nightShift) {
                if (trip.getType() == TripType.PICK_UP.getValue()) {
                    for (int j = 0; j < orderedEmployeeList.size() - 1; j++) {
                        if (orderedEmployeeList.get(j).getEmployee().getId()
                            .equals(thisEmp.getId())) {
                            boolean anyoneBoarded = false;
                            for (int k = j + 1; k < orderedEmployeeList.size() - 1; k++) {
                                if (EmpStatus.isBoarded(orderedEmployeeList.get(k).getStatus())) {
                                    anyoneBoarded = true;
                                    break;
                                }
                            }
                            if (!anyoneBoarded
                                && orderedEmployeeList.get(j + 1).getEmployee().getSex() == 1 && !(
                                EmpStatus.isDeboarded(orderedEmployeeList.get(j + 1).getStatus())
                                    || EmpStatus
                                    .isBoarded(orderedEmployeeList.get(j + 1).getStatus())
                                    || EmpStatus
                                    .isMissed(orderedEmployeeList.get(j + 1).getStatus()))) {
                                message = "Employee " + orderedEmployeeList.get(j + 1).getEmployee()
                                    .getName() + " is recognized as first female pickup on trip ID "
                                    + trip.getId() + " on Vehicle " + trip
                                    .getOperatorSubVendorVehicle().getVehicle()
                                    .getRegistrationNumber() + " due to Employee " + thisEmp
                                    .getName() + " did not boarded the cab, ";
                                alertService
                                    .generateTripEmployeeAlert(trip, orderedEmployeeList.get(j + 1),
                                        AlertClassification.ALERT_FIRST_PICKUP_FEMALE_IN_NO_SHOW,
                                        null, message);
                            }
                            break;
                        } else {
                            logger.info(
                                "Status of this employee" + orderedEmployeeList.get(j).getEmployee()
                                    .getName() + "is : " + orderedEmployeeList.get(j).getStatus());
                            if (EmpStatus.isBoarded(orderedEmployeeList.get(j).getStatus())) {
                                break;
                            } else {
                                continue;
                            }
                        }
                    }
                } else {
                    for (int j = orderedEmployeeList.size() - 1; j > 0; j--) {
                        if (orderedEmployeeList.get(j).getEmployee().getId()
                            .equals(thisEmp.getId())) {
                            if (orderedEmployeeList.get(j - 1).getEmployee().getSex() == 1 && !(
                                EmpStatus.isMissed(orderedEmployeeList.get(j - 1).getStatus())
                                    || EmpStatus
                                    .isDeboarded(orderedEmployeeList.get(j - 1).getStatus()))) {
                                message = "Employee " + orderedEmployeeList.get(j - 1).getEmployee()
                                    .getName() + " is recognized as last female drop on trip ID "
                                    + trip.getId() + " on Vehicle " + trip
                                    .getOperatorSubVendorVehicle().getVehicle()
                                    .getRegistrationNumber() + " due to Employee " + thisEmp
                                    .getName() + " did not boarded the cab, ";
                                alertService
                                    .generateTripEmployeeAlert(trip, orderedEmployeeList.get(j - 1),
                                        AlertClassification.ALERT_FIRST_PICKUP_FEMALE_IN_NO_SHOW,
                                        null, message);
                            }
                            break;
                        } else {
                            logger.info(
                                "Status of this employee" + orderedEmployeeList.get(j).getEmployee()
                                    .getName() + "is : " + orderedEmployeeList.get(j).getStatus());
                            if (EmpStatus.isBoarded(orderedEmployeeList.get(j).getStatus())) {
                                break;
                            } else {
                                continue;
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean firstTimeDeboarded(Trip trip, TripEmployee tripHasEmployee, Employee employee,
        String lastAddress, EventData eventData) {
        if (employee.getValidDropLatLong() < 2) {
            String message;
            employee.setDropLatitude(eventData.getLatitude());
            employee.setDropLongitude(eventData.getLongitude());
            employee.setValidDropLatLong((byte) 2);
            if (employee.getValidPickLatLong() < 2) {
                employee.setPickLatitude(eventData.getLatitude());
                employee.setPickLongitude(eventData.getLongitude());
            }
            message = "Emp " + employee.getName() + " (" + tripHasEmployee.getEmployee().getId()
                + ") has swiped access card for the first time at (" + lastAddress + ") on Trip ("
                + trip.getId() + ")";
            alertService.generateTripEmployeeAlert(trip, tripHasEmployee,
                AlertClassification.ALERT_EMP_FIRST_SWIPE, null, message);
            tripHasEmployee.setEndLatitude(eventData.getLatitude());
            tripHasEmployee.setEndLongitude(eventData.getLongitude());
            return true;
        } else {
            return false;
        }
    }

    private boolean firstTimeBoarded(Trip trip, TripEmployee tripHasEmployee, Employee employee,
        String lastAddress, EventData eventData) {
        if (employee.getValidPickLatLong() < 2) {
            String message;
            employee.setPickLatitude(eventData.getLatitude());
            employee.setPickLongitude(eventData.getLongitude());
            employee.setValidPickLatLong((byte) 2);
            if (employee.getValidDropLatLong() < 2) {
                employee.setDropLatitude(eventData.getLatitude());
                employee.setDropLongitude(eventData.getLongitude());
            }
            tripHasEmployee.setStartLatitude(eventData.getLatitude());
            tripHasEmployee.setStartLongitude(eventData.getLongitude());
            message = "Emp " + employee.getName() + " (" + tripHasEmployee.getEmployee().getId()
                + ") has swiped access card for the first time at (" + lastAddress + ") on Trip ("
                + trip.getId() + ")";
            alertService.generateTripEmployeeAlert(trip, tripHasEmployee,
                AlertClassification.ALERT_EMP_FIRST_SWIPE, null, message);
            return true;
        } else {
            return false;
        }
    }

    private void nextEmpByPassAlert(Trip trip, List<TripEmployee> orderedEmployeeList,
        Enum<EmpStatus> empStatus, Employee thisEmp, long eventTime) {
        String message = null;
        if (orderedEmployeeList.get(0).getTrip().getType() == TripType.PICK_UP.getValue()) {
            if ((empStatus == EmpStatus.BOARDED) || (empStatus == EmpStatus.FORCE_BOARDED) || (
                empStatus == EmpStatus.FORCE_MISSING) || (empStatus == EmpStatus.MISSING)) {
                for (int j = 0; j < orderedEmployeeList.size() - 1; j++) {
                    if (orderedEmployeeList.get(j).getEmployee().getId().equals(thisEmp.getId())) {
                        break;
                    } else {
                        logger.info(
                            "Status of this employee" + orderedEmployeeList.get(j).getEmployee()
                                .getName() + "is : " + orderedEmployeeList.get(j).getStatus());
                        if (EmpStatus.isBoarded(orderedEmployeeList.get(j).getStatus()) || EmpStatus
                            .isDeboarded(orderedEmployeeList.get(j).getStatus()) || EmpStatus
                            .isMissed(orderedEmployeeList.get(j).getStatus())
                            || (orderedEmployeeList.get(j).getNotificationStatus() & 0x1) != 0) {
                            continue;
                        } else {
                            message =
                                " Employee " + orderedEmployeeList.get(j).getEmployee().getName()
                                    + "(" + orderedEmployeeList.get(j).getEmployee().getId()
                                    + ") By-passed by driver " + trip.getOperatorSubVendorDriver()
                                    .getDriver().getName() + " in vehicle " + trip
                                    .getOperatorSubVendorVehicle().getVehicle()
                                    .getRegistrationNumber();
                            alertService.generateTripEmployeeAlert(trip, orderedEmployeeList.get(j),
                                AlertClassification.ALERT_EMP_BYPASSED, null, message);
                            orderedEmployeeList.get(j).setNotificationStatus(Utility
                                .setBit(orderedEmployeeList.get(j).getNotificationStatus(), 1));
                            tripHasEmployeeDAO.save(orderedEmployeeList.get(j));
                        }
                    }
                }
            }
        } else {
            if ((empStatus == EmpStatus.DEBOARDED) || (empStatus == EmpStatus.FORCE_DEBOARDED)) {
                for (int j = 0; j < orderedEmployeeList.size() - 1; j++) {
                    if (orderedEmployeeList.get(j).getEmployee().getId().equals(thisEmp.getId())) {
                        break;
                    } else {
                        logger.info(
                            "Status of this employee" + orderedEmployeeList.get(j).getEmployee()
                                .getName() + "is : " + orderedEmployeeList.get(j).getStatus());
                        if (EmpStatus.isDeboarded(orderedEmployeeList.get(j).getStatus())
                            || EmpStatus.isMissed(orderedEmployeeList.get(j).getStatus())
                            || (orderedEmployeeList.get(j).getNotificationStatus() & 0x1) > 0) {
                            continue;
                        } else {
                            message =
                                " Employee " + orderedEmployeeList.get(j).getEmployee().getName()
                                    + "(" + orderedEmployeeList.get(j).getEmployee().getId()
                                    + ") By-passed by driver " + trip.getOperatorSubVendorDriver()
                                    .getDriver().getName() + " in vehicle " + trip
                                    .getOperatorSubVendorVehicle().getVehicle()
                                    .getRegistrationNumber();
                            alertService.generateTripEmployeeAlert(trip, orderedEmployeeList.get(j),
                                AlertClassification.ALERT_EMP_BYPASSED, null, message);
                            orderedEmployeeList.get(j).setNotificationStatus(Utility
                                .setBit(orderedEmployeeList.get(j).getNotificationStatus(), 1));
                            tripHasEmployeeDAO.save(orderedEmployeeList.get(j));
                        }
                    }
                }
            }
        }
    }

    public enum EmpStatus {
        UNKNOWN,
        BOARDED,
        DEBOARDED,
        MISSING,
        ENTER_GEOZONE,
        EXIT_GEOZONE,
        WRONG_BOARDEDED,
        WRONG_DEBOARDEDED,
        NO_ZONE_BOARDEDED,
        NO_ZONE_DEBOARDED,
        INTIMATION,
        FORCE_INTIMATION,
        FORCE_BOARDED,
        FORCE_DEBOARDED,
        FORCE_MISSING;

        public static EmpStatus convert(int value) {
            return EmpStatus.values()[value];
        }

        public static boolean isMissed(int i) {
            return MISSING.ordinal() == i;
        }

        public static boolean isDeboarded(int i) {
            return DEBOARDED.ordinal() == i;
        }

        public static boolean isBoarded(int i) {
            return BOARDED.ordinal() == i;
        }
    }


}
