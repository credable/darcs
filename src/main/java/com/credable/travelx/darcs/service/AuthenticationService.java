package com.credable.travelx.darcs.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import com.credable.model.Driver;
import com.credable.travelx.darcs.cache.PropertiesCache;
import com.credable.travelx.darcs.dao.DriverDAO;

@Service
public class AuthenticationService {
    private final static Logger logger = LoggerFactory.getLogger(AuthenticationService.class);

    @Autowired
    private PropertiesCache propertiesCache;


    @Autowired
    private DriverDAO driverDAO;

    Set<String> excludedUrls = new HashSet<String>();

    public Boolean isExculdedUrl(String url) {
        url = url.substring(url.lastIndexOf("/"));
        logger.info("Search url " + url);
        excludedUrls.clear();
        String excUrlStr = propertiesCache.getProperties("darcs.app.excludedurls");
        if (excUrlStr != null && !excUrlStr.isEmpty()) {
            String[] excUrls = excUrlStr.split(",");
            if (excUrls != null && excUrls.length > 0)
                excludedUrls.addAll(Arrays.asList(excUrls));
        }
        if (excludedUrls.contains(url))
            return true;
        return false;
    }

    public Boolean validateApiRequest(HttpServletRequest request) throws Exception {
        logger.info("Authentication Service called. Validating request for API URL : " + request
            .getRequestURL());
        final String uuid = request.getHeader("x-auth-token");
        if (uuid != null && !uuid.isEmpty()) {
            return authenticateToken(uuid, request);
        } else {
            logger.error("Request not validated. Invalid or unathorized uuid availabe.");
            throw new HttpServerErrorException(HttpStatus.UNAUTHORIZED,
                "Request not valid. Authorization key not valid.");
        }
    }

    public Boolean authenticateToken(String token, HttpServletRequest request) throws Exception {
        if (token != null && !token.isEmpty()) {
            logger.debug("getting data from db for uuid {}", token);
            Driver driver = driverDAO.findByWebToken(token);
            if (driver != null) {
                request.setAttribute("driver", driver);
                return true;
            } else {
                throw new HttpServerErrorException(HttpStatus.UNAUTHORIZED,
                    "Request not valid. Authorization failed.");
            }
        }
        return false;
    }
}
