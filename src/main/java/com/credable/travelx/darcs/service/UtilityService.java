package com.credable.travelx.darcs.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.credable.model.Driver;
import com.credable.travelx.darcs.cache.AppConstants;
import com.credable.travelx.darcs.cache.PropertiesCache;
import com.credable.travelx.darcs.util.Email;

@Service
public class UtilityService {

    @Autowired
    PropertiesCache propertiesCache;
    @Autowired
    Email email;
    private static String UPLOADED_FOLDER = "temp/";

    private final static Logger logger = LoggerFactory.getLogger(UtilityService.class);

    public void logsUpload(List<MultipartFile> files, String detail, Driver driver) {
        logger.info("files uploaded:" + files.size() + " driver id:" + driver.getId());
        try {
            List<String> fileList = new ArrayList<>();
            File directory = new File(UPLOADED_FOLDER);
            if (!directory.exists()) {
                directory.mkdir();
            }
            for (MultipartFile file : files) {
                try {
                    if (!file.isEmpty()) {
                        logger.info("writing file:" + file.getOriginalFilename());
                        byte[] bytes = file.getBytes();
                        Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
                        Files.write(path, bytes);
                        fileList.add(path.toAbsolutePath().toString());
                    } else {
                        logger.info("empty file received:" + file.getOriginalFilename());
                    }
                } catch (IOException e) {
                    logger.error("IO exception while writing file", e);
                }
            }
            if (fileList.size() > 0) {
                boolean status = email
                    .send(propertiesCache.getProperties(AppConstants.LOG_MAILS), "Driver App Logs",
                        detail, fileList);
                if (status) {
                    logger.info("mail sent successfully..removing temp files");
                    for (String file : fileList) {
                        File f = new File(file);
                        if (f.exists())
                            f.delete();
                    }
                }
            }
        } catch (Exception e) {
            logger.error("mail exception", e);
        }
    }

    public void handleAppCrash(String stackTrace, Driver driver) {
        logger.info(stackTrace);
        try {
            email.send(propertiesCache.getProperties(AppConstants.LOG_MAILS),
                "Driver App Unchecked Exception", stackTrace, null);
        } catch (Exception e) {
            logger.error("mail exception", e);
        }
    }

}
