package com.credable.travelx.darcs.service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.credable.model.Driver;
import com.credable.travelx.darcs.cache.AppConstants;
import com.credable.travelx.darcs.cache.PropertiesCache;
import com.credable.travelx.darcs.dao.DriverDAO;
import com.credable.travelx.darcs.dto.DriverDTO;
import com.credable.travelx.darcs.pojo.DriverWM;
import com.credable.travelx.darcs.pojo.MetaDataWM;
import com.credable.util.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class DriverService {

    @Autowired
    private DriverDAO driverDAO;

    @Autowired
    private PushNotification pushNotification;

    private final static Logger logger = LoggerFactory.getLogger(DriverService.class);

    @Autowired
    private PropertiesCache propertiesCache;

    public ResponseEntity<?> checkForAppUpdate(int versionId) {
        int currentStableAppVersion =
            Integer.valueOf(propertiesCache.getProperties(AppConstants.APP_VERSION));
        if (currentStableAppVersion > versionId)
            return new ResponseEntity<String>(HttpStatus.FOUND.getReasonPhrase(), HttpStatus.FOUND);
        else
            return new ResponseEntity<String>(HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> sendMetaData(DriverWM driverWM) {
        MetaDataWM metaDataWM = new MetaDataWM();
        int currentStableAppVersion =
            Integer.valueOf(propertiesCache.getProperties(AppConstants.APP_VERSION));
        if (driverWM.getVersionId() < currentStableAppVersion) {
            metaDataWM.setAppOutDated(true);
            return new ResponseEntity<MetaDataWM>(metaDataWM, HttpStatus.OK);
        } else {
            if (driverWM.getId() != 0) {
                Driver driver = driverDAO.findByDriverId(driverWM.getId());
                if (driver != null) {
                    if (!driver.getManagementActive()) {
                        metaDataWM.setDriverActive(false);
                        return new ResponseEntity<MetaDataWM>(metaDataWM, HttpStatus.OK);
                    }
                    metaDataWM.setDriverActive(true);
                    driver.setAppVersion(driverWM.getVersionId());
                    driver.setDeviceInfo(driverWM.getDeviceInfo());
                    driverDAO.save(driver);
                    metaDataWM.setAppGpsAccuracyThreshold(driver.getGpsAccuracyThreshold());
                    metaDataWM.setAppLocationProvider(driver.getAppLocationProvider());
                }
            }
            metaDataWM.setLocationPacketRate(
                Integer.parseInt(propertiesCache.getProperties(AppConstants.LOCATION_RATE)));
            return new ResponseEntity<MetaDataWM>(metaDataWM, HttpStatus.OK);
        }

    }

    public ResponseEntity<?> verifyDriver(String contactNumber) {
        Driver driver = driverDAO.findByContactNumber(contactNumber);
        if (driver == null)
            return new ResponseEntity<String>(
                propertiesCache.getProperties(AppConstants.MSG_DRIVER_NOT_FOUND),
                HttpStatus.NOT_FOUND);

        HttpStatus status =
            driver.getPassword() == null ? HttpStatus.PRECONDITION_REQUIRED : HttpStatus.OK;

        if (status.equals(HttpStatus.PRECONDITION_REQUIRED)) {
            StringBuffer message = new StringBuffer();
            message.append("Dear ");
            message.append(driver.getName());
            message.append(" OTP for TravelX is ");
            StringBuilder generatedToken = new StringBuilder();
            try {
                SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
                for (int i = 0; i < 4; i++) {
                    generatedToken.append(number.nextInt(9));
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            driver.setToken(generatedToken.toString());
            driverDAO.save(driver);
            message.append(generatedToken.toString());
            logger.info(message.toString());
            Utility.sendSms(driver.getContactNumber(), message.toString(), false);
        }
        return new ResponseEntity<String>(status.getReasonPhrase(), status);
    }

    public ResponseEntity<?> signUpDriver(String contactNumber, DriverWM driverWM) {
        Driver driver = driverDAO.findByContactNumber(contactNumber);
        if (driver == null)
            return new ResponseEntity<String>(
                propertiesCache.getProperties(AppConstants.MSG_DRIVER_NOT_FOUND),
                HttpStatus.NOT_FOUND);

        if (!driverWM.getOtp().equals(driver.getToken()))
            return new ResponseEntity<String>(
                propertiesCache.getProperties(AppConstants.MSG_WRONG_OTP), HttpStatus.UNAUTHORIZED);

        driver.setPassword(driverWM.getPassword());
        driver.setWebToken(UUID.randomUUID().toString());
        driverDAO.save(driver);

        DriverDTO driverDTO = new DriverDTO().map(driver);
        return new ResponseEntity<DriverDTO>(driverDTO, HttpStatus.OK);
    }

    public ResponseEntity<?> loginDriver(String contactNumber, DriverWM driverWM) {
        Driver driver = driverDAO.findByContactNumber(contactNumber);
        if (driver == null)
            return new ResponseEntity<String>(
                propertiesCache.getProperties(AppConstants.MSG_DRIVER_NOT_FOUND),
                HttpStatus.NOT_FOUND);

        if (!driverWM.getPassword().equals(driver.getPassword()))
            return new ResponseEntity<String>(
                propertiesCache.getProperties(AppConstants.MSG_WRONG_OTP), HttpStatus.UNAUTHORIZED);

        driver.setWebToken(UUID.randomUUID().toString());
        driverDAO.save(driver);

        DriverDTO driverDTO = new DriverDTO().map(driver);
        return new ResponseEntity<DriverDTO>(driverDTO, HttpStatus.OK);
    }

    public ResponseEntity<?> setDriverPassword(Driver driver, DriverWM driverWM) {
        if (!driver.getPassword().equals(driverWM.getOldPassword()))
            return new ResponseEntity<String>(HttpStatus.FAILED_DEPENDENCY.getReasonPhrase(),
                HttpStatus.FAILED_DEPENDENCY);

        driver.setPassword(driverWM.getPassword());
        driver.setWebToken(UUID.randomUUID().toString());
        driverDAO.save(driver);

        DriverDTO driverDTO = new DriverDTO().map(driver);
        return new ResponseEntity<DriverDTO>(driverDTO, HttpStatus.OK);
    }

    public ResponseEntity<?> forgotPassword(String contactNumber) {
        Driver driver = driverDAO.findByContactNumber(contactNumber);
        if (driver == null)
            return new ResponseEntity<String>(
                propertiesCache.getProperties(AppConstants.MSG_DRIVER_NOT_FOUND),
                HttpStatus.NOT_FOUND);

        StringBuffer message = new StringBuffer();
        message.append("Dear ");
        message.append(driver.getName());
        message.append(" OTP for TravelX is ");
        StringBuilder generatedToken = new StringBuilder();
        try {
            SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
            for (int i = 0; i < 4; i++) {
                generatedToken.append(number.nextInt(9));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        driver.setToken(generatedToken.toString());
        driverDAO.save(driver);
        message.append(generatedToken.toString());
        logger.info(message.toString());
        Utility.sendSms(driver.getContactNumber(), message.toString(), false);
        return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

    public ResponseEntity<?> logoutDriver(Driver driver) {
        if (driver == null)
            return new ResponseEntity<String>(
                propertiesCache.getProperties(AppConstants.MSG_DRIVER_NOT_FOUND),
                HttpStatus.NOT_FOUND);

        driver.setWebToken(null);
        driver.setToken(null);
        driverDAO.save(driver);
        return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

    public ResponseEntity<?> setDriverToken(Driver driver, DriverWM driverWM) {
        if (driver == null)
            return new ResponseEntity<String>(
                propertiesCache.getProperties(AppConstants.MSG_DRIVER_NOT_FOUND),
                HttpStatus.NOT_FOUND);

        driver.setToken(driverWM.getToken());
        driverDAO.save(driver);
        return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
    }

    public ResponseEntity<?> getDriverLogs(int driverId, int duration) {
        try {
            Driver driver = driverDAO.findByDriverId(driverId);
            if (driver != null && driver.getToken() != null) {
                ObjectMapper mapper = new ObjectMapper();
                ObjectNode body = mapper.createObjectNode();
                body.put("priority", "high");
                body.put("to", driver.getToken());
                ObjectNode data = mapper.createObjectNode();
                data.put("logs", true);
                data.put("duration", duration);
                body.set("data", data);
                HttpEntity<String> request = new HttpEntity<>(body.toString());
                logger.info(body.toString());
                pushNotification.send(request);
                return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(), HttpStatus.OK);
            } else {
                return new ResponseEntity<String>("Driver or FCM token not found",
                    HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Something went wrong",
                HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
