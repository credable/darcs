alter table  driver add column app_location_provider enum ('google','gps') default 'google';
alter table driver add column device_info varchar(255) default null ;
alter table driver add column gps_accuracy_threshold int(11) not null  default 25;
alter table driver add column app_version int(11) default null;
alter table trip add column trip_start_window int(11) not null default 60;

CREATE TABLE `system_property` (
 `name` varchar(255) NOT NULL,
 `value` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`name`)
);

insert into system_property values("appLogMail","abhishek.trivedi@credable.in");
insert into system_property values("appServiceMail","travelxservice@gmail.com");
insert into system_property values("appServicePass","gquditdulwoxqoyd");
insert into system_property values("appLocationPacketRate","10");
insert into system_property values("passcode","credable@01");
update driver_compliance set management_active = 1;